import os
import shutil
import random

def split_folder_into_train_test(source_folder, train_folder, test_folder, proportion):
    # Create destination folders for train and test sets
    os.makedirs(train_folder, exist_ok=True)
    os.makedirs(test_folder, exist_ok=True)

    # Get the list of subfolders in the source folder
    subfolders = [f for f in os.listdir(source_folder) if os.path.isdir(os.path.join(source_folder, f))]
    print(subfolders)
    
    # Iterate through each subfolder
    for subfolder in subfolders:
        source_subfolder = os.path.join(source_folder, subfolder)
        train_subfolder = os.path.join(train_folder, subfolder)
        test_subfolder = os.path.join(test_folder, subfolder)
        os.makedirs(train_subfolder, exist_ok=True)
        os.makedirs(test_subfolder, exist_ok=True)
        
        # Get the list of files in the subfolder
        files = os.listdir(source_subfolder)
        
        # Shuffle the files randomly
        random.shuffle(files)
        
        # Calculate the number of files to be included in the train set
        num_files = len(files)
        num_train_files = int(num_files * proportion)
        
        # Move files to the train set
        train_files = files[:num_train_files]
        for file in train_files:
            source_file = os.path.join(source_subfolder, file)
            destination_file = os.path.join(train_subfolder, file)
            shutil.copy2(source_file, destination_file)
        
        # Move remaining files to the test set
        test_files = files[num_train_files:]
        for file in test_files:
            source_file = os.path.join(source_subfolder, file)
            destination_file = os.path.join(test_subfolder, file)
            shutil.copy2(source_file, destination_file)




def sort_files_by_eighth_character(folder_path):
    for i in range(10):
        folder_name = f"/home/pibou/Documents/M2DS/RP/datasets/Folder_{i}"
        os.makedirs(folder_name, exist_ok=True)
    
    # Get all files in the folder
    files = os.listdir(folder_path)
    
    # Sort files into appropriate folders based on the third character
    for file in files:
        if os.path.isfile(os.path.join(folder_path, file)):
            third_character = file[7]
            destination_folder = f"/home/pibou/Documents/M2DS/RP/datasets/Folder_{third_character}"
            shutil.move(os.path.join(folder_path, file), destination_folder)


folder_path = "/home/pibou/Documents/M2DS/RP/datasets/CK+_TIM10_processed/"
#sort_files_by_eighth_character(folder_path)

source_folder = "/home/pibou/Documents/M2DS/RP/datasets/CK+_TIM10_sorted"
# Provide the path to the destination folders for the train and test sets
train_folder = "/home/pibou/Documents/M2DS/RP/datasets/CK+_TIM10_final/train"
test_folder = "/home/pibou/Documents/M2DS/RP/datasets/CK+_TIM10_final/test"
# Specify the proportion of files to be included in the train set (between 0 and 1)
proportion = 0.8

split_folder_into_train_test(source_folder, train_folder, test_folder, proportion)