import os
import cv2
import numpy as np


def perform_operation(input_path, output_path):
    for folder_name in os.listdir(input_path):
        folder_path = os.path.join(input_path, folder_name)
        for subfolder_name in os.listdir(folder_path):
            subfolder_path = os.path.join(folder_path, subfolder_name)
            if os.path.isdir(subfolder_path):

                image_files = []
                for file_name in os.listdir(subfolder_path):
                    if file_name.endswith(".jpg") or file_name.endswith(".png"):
                        image_files.append(os.path.join(subfolder_path, file_name))
                image_files.sort()

                image1 = cv2.imread(image_files[0], 0)
                image2 = cv2.imread(image_files[-1], 0)
                flow = cv2.calcOpticalFlowFarneback(image1, image2, None, 0.5, 3, 15, 3, 5, 1.2, 0)

                hsv = np.zeros((image1.shape[0], image1.shape[1], 3), dtype=np.uint8)
                mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
                hsv[..., 0] = ang * 180 / np.pi / 2
                hsv[..., 1] = 255
                hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)



                flow_name = folder_name + '_' + subfolder_name + ".png"
                cv2.imwrite(os.path.join(output_path, flow_name), hsv)
    return


input_path = "/home/pibou/Documents/M2DS/RP/datasets/CK+_TIM10/"
output_path = "/home/pibou/Documents/M2DS/RP/datasets/CK+_TIM10_processed/"
perform_operation(input_path, output_path)